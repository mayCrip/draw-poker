﻿using System;
using DrawPoker.Core.Interfaces;
using DrawPoker.Core.Entities;
using DrawPoker.Core.Enums;

namespace DrawPoker.Core.CardSelectionRules
{
    public class ExactFaceRule : ICardSelectionRule
    {
        private readonly Func<Card, bool> _predicate;

        public ExactFaceRule(Faces face)
        {
            _predicate = new Func<Card, bool>(x => x.Face == face);
        }

        public Func<Card, bool> GetPredicate()
        {
            return _predicate;
        }
    }
}
