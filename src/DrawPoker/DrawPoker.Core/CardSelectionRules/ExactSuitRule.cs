﻿using System;
using DrawPoker.Core.Interfaces;
using DrawPoker.Core.Entities;
using DrawPoker.Core.Enums;

namespace DrawPoker.Core.CardSelectionRules
{
    public class ExactSuitRule : ICardSelectionRule
    {
        private readonly Func<Card, bool> _predicate;

        public ExactSuitRule(Suits suit)
        {
            _predicate = new Func<Card, bool>(x => x.Suit == suit);
        }

        public Func<Card, bool> GetPredicate()
        {
            return _predicate;
        }
    }
}
