﻿using System;
using DrawPoker.Core.Interfaces;
using DrawPoker.Core.Entities;
using DrawPoker.Core.Enums;

namespace DrawPoker.Core.CardSelectionRules
{
    public class DescSeqLoopFaceRule : ICardSelectionRule
    {
        private Faces _currentFace;

        public DescSeqLoopFaceRule(Faces startFace)
        {
            this._currentFace = startFace;
        }

        public Func<Card, bool> GetPredicate()
        {
            var filterFace = this._currentFace;
            var predicate = new Func<Card, bool>(x => x.Face == filterFace);

            this._currentFace = (int)this._currentFace > 2 ? this._currentFace - 1 : Faces.Ace;

            return predicate;
        }
    }
}
