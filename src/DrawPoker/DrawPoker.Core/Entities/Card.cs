﻿using DrawPoker.Core.Enums;
using System;

namespace DrawPoker.Core.Entities
{
    public struct Card
    {
        public Card(Faces face, Suits? suit)
        {
            this.Face = face;
            this.Suit = suit;
        }

        public Suits? Suit { get; set; }

        public Faces Face { get; set; }

        public override string ToString()
        {
            return String.Format("{0} of {1}", Enum.GetName(typeof(Faces), this.Face), Enum.GetName(typeof(Suits), this.Suit.Value));
        }
    }
}