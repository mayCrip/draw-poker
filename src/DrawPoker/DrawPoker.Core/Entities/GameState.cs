﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DrawPoker.Core.Interfaces;

namespace DrawPoker.Core.Entities
{
    public class GameState
    {
        public Dictionary<Guid, Card[]> PlayerCardDistribution { get; set; }
    }
}
