﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DrawPoker.Core.Interfaces;
using DrawPoker.Core.Enums;

namespace DrawPoker.Core.Entities
{
    public class Deck : IDeck
    {
        private readonly IShuffleStrategy _shuffleStrategy;
        private readonly int _jokersCount = 0;
        private HashSet<Card> _cards = new HashSet<Card>();

        public Deck(IShuffleStrategy shuffleStrategy, int jokersCount)
        {
            this._shuffleStrategy = shuffleStrategy;
            this._jokersCount = jokersCount;

            this.InitializeDeck();
        }

        private void InitializeDeck()
        {
            foreach (var face in Enum.GetValues(typeof(Faces)).Cast<Faces>())
            {
                if (face == Faces.Joker) continue;
                
                foreach (var suit in Enum.GetValues(typeof(Suits)).Cast<Suits>())
                {
                    this._cards.Add(new Card(face, suit));
                }
            }

            for (int i = 0; i < this._jokersCount; i++)
            {
                this._cards.Add(new Card(Faces.Joker, null));
            }
        }

        public void Reset()
        {
            this.InitializeDeck();
        }

        public IEnumerable<Card>[] ChangeCards(IDictionary<Guid, int> changeMap)
        {
            var result = new List<IEnumerable<Card>>();
            foreach (var playerId in changeMap.Keys)
            {
                var replacements = _shuffleStrategy
                    .ChangeCards(this._cards.ToArray(), playerId, changeMap[playerId]);
                this._cards.ExceptWith(replacements);
                result.Add(replacements);
            }

            return result.ToArray();
        }

        public IEnumerable<Card>[] DistributeCards(ICollection<IPlayer> players)
        {
            var distribution = new List<IEnumerable<Card>>();

            foreach (var player in players)
            {
                var hand = this._shuffleStrategy.ProvideCards(this._cards.ToArray(), player);
                this._cards.ExceptWith(hand);
                distribution.Add(hand);
            }

            return distribution.ToArray();
        }
    }
}
