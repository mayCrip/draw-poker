﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DrawPoker.Core.Interfaces;

namespace DrawPoker.Core.Entities
{
    public class Player: IPlayer
    {

        public Player(bool isBot)
        {
            this.IsBot = isBot;
            this.Id = Guid.NewGuid();
        }

        public Guid Id { get; set; }

        public bool IsBot { get; set; }
    }
}
