﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DrawPoker.Core.Entities;
using DrawPoker.Core.Interfaces;

namespace DrawPoker.Core.Entities
{
    public class Dealer
    {
        private readonly IPlayer[] _players;
        private readonly IDeck _deck;

        public Dealer(IPlayer[] players, IDeck deck)
        {
            this._players = players;
            this._deck = deck;
        }

    }
}
