﻿using System.Collections.Generic;
using System.Linq;
using DrawPoker.Core.Interfaces;

namespace DrawPoker.Core.Entities
{
    public class CardGroup : ICardGroup
    {
        private readonly int _cardsCount;
        private readonly ICardPicker _cardPicker;

        public CardGroup(int cardsCount, ICardPicker picker)
        {
            this._cardsCount = cardsCount;
            this._cardPicker = picker;
        }

        public IEnumerable<Card> GetCards(IList<Card> avaiableCards)
        {
            if (avaiableCards.Count < this._cardsCount)
            {
                return null;
            }

            var cards = new List<Card>();
            var availableCardsSet = new HashSet<Card>(avaiableCards);

            for (var i = 0; i < this._cardsCount; i++)
            {
                var pickedCard = this._cardPicker.Pick(availableCardsSet.ToList());
                if (pickedCard.HasValue)
                {
                    cards.Add(pickedCard.Value);
                    availableCardsSet.Remove(pickedCard.Value);
                }
                else
                {
                    return null;
                }
            }

            return cards.AsEnumerable();
        }
    }
}