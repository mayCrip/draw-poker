﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DrawPoker.Core.Interfaces;
using DrawPoker.Core.Enums;
using DrawPoker.Core.GameEventArgs;

namespace DrawPoker.Core.Entities
{

    public class SingleModeGame: IDrawPokerGame<UserRequests, UserActions>
    {
        public event EventHandler<IActionRequestEventArgs<UserRequests>> ActionRequested;
        public event EventHandler<GameStateChangedEventArgs> GameStateChanged;

        public void Dispatch(IUserAction<UserActions> userAction)
        {
            throw new NotImplementedException();
        }

        public void Start(int botCount, int jokerCount, IShuffleStrategy shuffleStrategy)
        {
            throw new NotImplementedException();
        }

        private Player[] GeneratePlayers(int botCount)
        {
            var players = new List<Player>();

            players.Add(new Player(false));

            for (int i = 0; i < botCount; i++)
            {
                players.Add(new Player(true));
            }

            return players.ToArray();
        }
    }
}
