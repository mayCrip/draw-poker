﻿namespace DrawPoker.Core.Enums
{
    public enum Suits
    {
        Diamonds,
        Hearts,
        Spades,
        Clubs
    }
}
