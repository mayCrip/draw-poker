﻿namespace DrawPoker.Core.Enums
{
    public enum UserActions
    {
        ChangeCards,
        SkipChanging,
        NextRound,
        Quit
    }
}
