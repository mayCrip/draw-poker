﻿using System;
using System.Collections.Generic;
using DrawPoker.Core.Entities;

namespace DrawPoker.Core.Interfaces
{
    public interface IShuffleStrategy
    {
        IEnumerable<Card> ChangeCards(IList<Card> cards, Guid playerId, int changeCardNumber);

        IEnumerable<Card> ProvideCards(IList<Card> cards, IPlayer player);
    }
}
