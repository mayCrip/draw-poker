﻿using System;
using System.Collections.Generic;
using DrawPoker.Core.Entities;

namespace DrawPoker.Core.Interfaces
{
    public interface IDeck
    {
        IEnumerable<Card>[] DistributeCards(ICollection<IPlayer> players);

        IEnumerable<Card>[] ChangeCards(IDictionary<Guid, int> changeMap);

        void Reset();
    }
}
