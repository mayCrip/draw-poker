﻿using System;
using DrawPoker.Core.Entities;

namespace DrawPoker.Core.Interfaces
{
    public interface ICardSelectionRule
    {
        Func<Card, bool> GetPredicate();
    }
}
