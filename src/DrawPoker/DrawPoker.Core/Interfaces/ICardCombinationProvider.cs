﻿using System.Collections.Generic;
using DrawPoker.Core.Entities;

namespace DrawPoker.Core.Interfaces
{
    public interface ICardCombinationProvider
    {
        IEnumerable<Card> GetCombination(IList<Card> cards);
    }
}
