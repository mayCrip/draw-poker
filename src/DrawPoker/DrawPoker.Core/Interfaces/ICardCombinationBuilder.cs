﻿using System.Collections.Generic;
using DrawPoker.Core.Entities;

namespace DrawPoker.Core.Interfaces
{
    public interface ICardCombinationBuilder
    {
        IEnumerable<Card> Build(IList<Card> cardsAvailable);
    }
}
