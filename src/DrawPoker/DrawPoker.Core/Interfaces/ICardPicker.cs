﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DrawPoker.Core.Entities;

namespace DrawPoker.Core.Interfaces
{
    public interface ICardPicker
    {
        Card? Pick(IList<Card> cards);
    }
}
