﻿namespace DrawPoker.Core.Interfaces
{
    public interface ICombinationBuilderFactory
    {
        ICardCombinationBuilder GetBuilder();
    }
}
