﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawPoker.Core.Interfaces
{
    public interface IPlayer
    {
        Guid Id { get; set; }

        bool IsBot { get; set; }
    }
}
