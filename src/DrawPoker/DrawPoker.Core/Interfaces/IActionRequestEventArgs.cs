﻿namespace DrawPoker.Core.Interfaces
{
    public interface IActionRequestEventArgs<TUserRequests>
        where TUserRequests : struct
    {
        TUserRequests Request { get; set; }
    }
}
