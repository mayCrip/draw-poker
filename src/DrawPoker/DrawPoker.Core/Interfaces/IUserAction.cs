﻿namespace DrawPoker.Core.Interfaces
{
    public interface IUserAction<TUserActions>
        where TUserActions : struct
    {
        TUserActions Action { get; set; }

        object Data { get; set; }
    }
}
