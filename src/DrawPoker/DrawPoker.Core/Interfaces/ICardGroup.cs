﻿using System.Collections.Generic;
using DrawPoker.Core.Entities;

namespace DrawPoker.Core.Interfaces
{
    public interface ICardGroup
    {
        IEnumerable<Card> GetCards(IList<Card> availableCards);
    }
}
