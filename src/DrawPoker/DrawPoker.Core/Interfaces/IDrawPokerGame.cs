﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DrawPoker.Core.GameEventArgs;

namespace DrawPoker.Core.Interfaces
{
    public interface IDrawPokerGame<TUserRequests, TUserActions>
        where TUserRequests : struct
        where TUserActions : struct
    {
        event EventHandler<IActionRequestEventArgs<TUserRequests>> ActionRequested;

        event EventHandler<GameStateChangedEventArgs> GameStateChanged;

        void Dispatch(IUserAction<TUserActions> userAction);

        void Start(int botCount, int jokerCount, IShuffleStrategy shuffleStrategy);
    }
}
