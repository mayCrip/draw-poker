﻿using System;
using System.Collections.Generic;
using System.Linq;
using DrawPoker.Core.Entities;
using DrawPoker.Core.Interfaces;
using DrawPoker.Core.Enums;
using DrawPoker.Core.CardCombinationBuilders;

namespace DrawPoker.Core.ShuffleStrategies
{
    public class StrongHandStrategy : IShuffleStrategy
    {
        private readonly ICardCombinationProvider _combinationProvider;
        private readonly ICardCombinationProvider _randomCombinationProvider =
            new RandomCardCombinationProvider();
        private readonly IDictionary<Guid, Card[]> _playerReservedCards =
            new Dictionary<Guid, Card[]>();

        public StrongHandStrategy(ICardCombinationProvider combinationProvider)
        {
            this._combinationProvider = combinationProvider;
        }

        public IEnumerable<Card> ChangeCards(IList<Card> cards, Guid playerId, int changeCardNumber)
        {
            if (this._playerReservedCards.ContainsKey(playerId))
            {
                var storedLenght = this._playerReservedCards[playerId].Length;


                if (this._playerReservedCards[playerId].Length >= changeCardNumber)
                {
                    return this._playerReservedCards[playerId].Take(changeCardNumber);
                }
                else
                {
                    var res = new List<Card>(this._playerReservedCards[playerId]);
                    var randomHand = this._randomCombinationProvider
                        .GetCombination(cards)
                        .Take(changeCardNumber - storedLenght);
                    res.AddRange(randomHand);

                    return res;
                } 
            }

            return _randomCombinationProvider
                .GetCombination(cards)
                .Take(changeCardNumber);
        }

        public IEnumerable<Card> ProvideCards(IList<Card> cards, IPlayer player)
        {
            if (!player.IsBot)
            {
                var cardSet = new HashSet<Card>(cards);
                var hand = _combinationProvider.GetCombination(cards).ToArray();

                cardSet.ExceptWith(hand);

                var randomHand = _randomCombinationProvider.GetCombination(cardSet.ToList()).ToArray();
                var handRandomPick = this.RandomPick(hand, 2);
                var handReplacement = this.RandomPick(randomHand, 2);
                var replacements = handReplacement.Select(x => randomHand[x]).ToArray();

                _playerReservedCards.Add(player.Id, handRandomPick.Select(x => hand[x]).ToArray());

                var i = 0;
                foreach (var ind in handRandomPick)
                {
                    hand[ind] = replacements[i];
                    i++;
                }

                return hand;
            }

            return _randomCombinationProvider.GetCombination(cards);
        }

        private IEnumerable<int> RandomPick(IEnumerable<Card> cards, int picksCount)
        {
            var rnd = new Random();
            var cardsCount = cards.Count();
            var reserved = new List<int>();

            for (int i = 0; i < picksCount; i++)
            {
                var pick = rnd.Next(cardsCount);

                while (reserved.Contains(pick))
                {
                    pick = rnd.Next(cardsCount);
                }

                reserved.Add(pick);
            }

            return reserved;
        }
    }
}
