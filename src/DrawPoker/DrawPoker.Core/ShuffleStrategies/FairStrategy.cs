﻿using System;
using System.Collections.Generic;
using System.Linq;

using DrawPoker.Core.Interfaces;
using DrawPoker.Core.Entities;
using DrawPoker.Core.CardCombinationBuilders;

namespace DrawPoker.Core.ShuffleStrategies
{
    public class FairStrategy : IShuffleStrategy
    {
        private const int HAND_CARDS_COUNT = 5;

        private readonly ICardCombinationProvider _cardCombinationProvider = new RandomCardCombinationProvider();

        public IEnumerable<Card> ChangeCards(IList<Card> cards, Guid playerId, int changeCardNumber)
        {
            var candidates = cards.Count < 5 ?
                cards :
                new List<Card>(_cardCombinationProvider.GetCombination(cards));

            var randomPicked = this.RandomPick(new List<Card>(candidates), changeCardNumber);

            return randomPicked.Select(x => candidates[x]);
        }

        private IEnumerable<int> RandomPick(IList<Card> cards, int picksCount)
        {
            var rnd = new Random();
            var reserved = new List<int>();

            for (int i = 0; i < picksCount; i++)
            {
                var pick = rnd.Next(cards.Count);

                while (reserved.Contains(pick))
                {
                    pick = rnd.Next(cards.Count);
                }

                reserved.Add(pick);
            }

            return reserved;
        }

        public IEnumerable<Card> ProvideCards(IList<Card> cards, IPlayer player)
        {
            return _cardCombinationProvider.GetCombination(cards);
        }
    }
}
