﻿using System.Collections.Generic;
using System.Linq;
using DrawPoker.Core.Entities;
using DrawPoker.Core.Interfaces;
using DrawPoker.Core.CardCombinationBuilders.Factories;

namespace DrawPoker.Core.CardCombinationBuilders
{
    public class RandomCardCombinationProvider : ICardCombinationProvider
    {
        private const int attempts = 5;
        private readonly ICombinationBuilderFactory _randomFactory = new RandomBuilderFactory();

        public IEnumerable<Card> GetCombination(IList<Card> cards)
        {
            var fallbackBuilder = this._randomFactory.GetBuilder();
            return fallbackBuilder.Build(cards.ToList());
        }
    }
}
