﻿using DrawPoker.Core.Entities;
using DrawPoker.Core.Enums;
using DrawPoker.Core.CardSelectionRules;
using DrawPoker.Core.CardPickers;

namespace DrawPoker.Core.CardCombinationBuilders
{
    public class FullHouseBuilder : CardCombinationBuilderBase
    {
        public FullHouseBuilder(Faces majorFace, Faces minorFace) : base()
        {
            this.AddGroup(new CardGroup(3, new StrictCardPicker(new[] { new ExactFaceRule(majorFace) })));
            this.AddGroup(new CardGroup(2, new StrictCardPicker(new[] { new ExactFaceRule(minorFace) })));
        }
    }
}
