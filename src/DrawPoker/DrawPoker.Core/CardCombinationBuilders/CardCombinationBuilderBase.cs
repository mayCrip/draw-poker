﻿using System.Collections.Generic;
using System.Linq;
using DrawPoker.Core.Entities;
using DrawPoker.Core.Interfaces;

namespace DrawPoker.Core.CardCombinationBuilders
{
    public class CardCombinationBuilderBase : ICardCombinationBuilder
    {
        private readonly IList<ICardGroup> _cardGroups;

        public CardCombinationBuilderBase()
        {
            this._cardGroups = new List<ICardGroup>();
        }

        protected void AddGroup(ICardGroup cardGroup)
        {
            this._cardGroups.Add(cardGroup);
        }

        public IEnumerable<Card> Build(IList<Card> cards)
        {
            var combination = new List<Card>();

            foreach (var cardGroup in this._cardGroups)
            {
                var groupCards = cardGroup.GetCards(cards);
                
                if (groupCards == null)
                {
                    return null;
                }

                combination.AddRange(groupCards);
            }

            return combination.AsEnumerable();
        }
    }
}
