﻿using System.Collections.Generic;
using System.Linq;
using DrawPoker.Core.Entities;
using DrawPoker.Core.Interfaces;
using DrawPoker.Core.CardCombinationBuilders.Factories;

namespace DrawPoker.Core.CardCombinationBuilders
{
    public class StrongCombinationProvider : ICardCombinationProvider
    {
        private const int attempts = 5;
        private readonly ICombinationBuilderFactory _fallbackFactory = new RandomBuilderFactory();
        private readonly IEnumerable<ICombinationBuilderFactory> _factories = new ICombinationBuilderFactory[]
        {
            new FullHouseBuilderFactory(),
            new RoyalFlushBuilderFactory()
        };

        public IEnumerable<Card> GetCombination(IList<Card> cards)
        {
            foreach (var factory in this._factories)
            {
                for (int i = 0; i < 5; i++)
                {
                    var builder = factory.GetBuilder();
                    var combination = builder.Build(cards.ToList());

                    if (combination != null) return combination;
                }
            }

            var fallbackBuilder = this._fallbackFactory.GetBuilder();
            return fallbackBuilder.Build(cards.ToList());
        }
    }
}
