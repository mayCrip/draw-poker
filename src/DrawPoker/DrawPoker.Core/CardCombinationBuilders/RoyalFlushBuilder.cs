﻿using DrawPoker.Core.Entities;
using DrawPoker.Core.Enums;
using DrawPoker.Core.CardPickers;
using DrawPoker.Core.CardSelectionRules;
using DrawPoker.Core.Interfaces;

namespace DrawPoker.Core.CardCombinationBuilders
{
    public class RoyalFlushBuilder : CardCombinationBuilderBase
    {
        public RoyalFlushBuilder(Suits suit) : base()
        {
            var selectionRules = new ICardSelectionRule[]
            {
                new DescSeqLoopFaceRule(Faces.Ace),
                new ExactSuitRule(suit)
            };

            this.AddGroup(new CardGroup(5, new StrictCardPicker(selectionRules)));
        }
    }
}
