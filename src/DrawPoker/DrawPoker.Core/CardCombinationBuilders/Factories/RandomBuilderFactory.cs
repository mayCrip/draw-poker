﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DrawPoker.Core.Interfaces;

namespace DrawPoker.Core.CardCombinationBuilders.Factories
{
    public class RandomBuilderFactory : ICombinationBuilderFactory
    {
        public ICardCombinationBuilder GetBuilder()
        {
            return new RandomCombinationBuilder();
        }
    }
}
