﻿using System;
using DrawPoker.Core.Interfaces;
using DrawPoker.Core.Enums;

namespace DrawPoker.Core.CardCombinationBuilders.Factories
{
    public class FullHouseBuilderFactory : ICombinationBuilderFactory
    {
        private readonly Random _rnd = new Random();

        public ICardCombinationBuilder GetBuilder()
        {
            var faceMajor = (Faces)_rnd.Next(2, 15);
            var faceMinor = (Faces)_rnd.Next(2, 15);

            while (faceMinor == faceMajor)
            {
                faceMinor = (Faces)_rnd.Next(2, 15);
            }

            return new FullHouseBuilder(faceMajor, faceMinor);
        }
    }
}
