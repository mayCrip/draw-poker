﻿using System;
using DrawPoker.Core.Interfaces;
using DrawPoker.Core.Enums;

namespace DrawPoker.Core.CardCombinationBuilders.Factories
{
    public class RoyalFlushBuilderFactory : ICombinationBuilderFactory
    {
        private readonly Random _rnd = new Random();

        public ICardCombinationBuilder GetBuilder()
        {
            var suit = (Suits)_rnd.Next(4);

            return new RoyalFlushBuilder(suit);
        }
    }
}
