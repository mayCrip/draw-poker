﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DrawPoker.Core.Entities;
using DrawPoker.Core.CardPickers;

namespace DrawPoker.Core.CardCombinationBuilders
{
    public class RandomCombinationBuilder: CardCombinationBuilderBase
    {
        public RandomCombinationBuilder()
        {
            this.AddGroup(new CardGroup(5, new RandomCardPicker()));
        }
    }
}
