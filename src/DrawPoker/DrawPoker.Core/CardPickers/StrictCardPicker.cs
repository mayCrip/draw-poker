﻿using System;
using System.Linq;
using System.Collections.Generic;
using DrawPoker.Core.Interfaces;
using DrawPoker.Core.Entities;

namespace DrawPoker.Core.CardPickers
{
    public class StrictCardPicker: ICardPicker
    {
        private readonly IEnumerable<ICardSelectionRule> _rules;
        private readonly Random _rnd = new Random();

        public StrictCardPicker(IEnumerable<ICardSelectionRule> rules)
        {
            this._rules = rules;
        }

        public Card? Pick(IList<Card> availableCards)
        {
            IList<Card> cardsLeft = new List<Card>(availableCards);

            foreach (var rule in this._rules)
            {
                cardsLeft = cardsLeft.Where(rule.GetPredicate()).ToList();
            }

            return cardsLeft != null && cardsLeft.Count > 0 ? cardsLeft[_rnd.Next(cardsLeft.Count)] : (Card?)null;
        }
    }
}
