﻿using System;
using System.Collections.Generic;
using DrawPoker.Core.Interfaces;
using DrawPoker.Core.Entities;

namespace DrawPoker.Core.CardPickers
{
    public class RandomCardPicker: ICardPicker
    {
        private readonly Random _rnd = new Random();

        public Card? Pick(IList<Card> availableCards)
        {
            return availableCards[_rnd.Next(availableCards.Count)];
        }
    }
}
