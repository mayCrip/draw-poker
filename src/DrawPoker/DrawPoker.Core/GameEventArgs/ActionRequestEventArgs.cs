﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DrawPoker.Core.Interfaces;
using DrawPoker.Core.Enums;

namespace DrawPoker.Core.GameEventArgs
{
    public class ActionRequestEventArgs : IActionRequestEventArgs<UserRequests>
    {
        public UserRequests Request { get; set; }
    }
}
