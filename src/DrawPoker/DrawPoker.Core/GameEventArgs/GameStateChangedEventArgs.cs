﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawPoker.Core.GameEventArgs
{
    public class GameStateChangedEventArgs
    {
        public int GameState { get; set; }
    }
}
