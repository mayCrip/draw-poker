﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DrawPoker.Core.Entities;

namespace DrawPoker.ConsoleClient
{
    class Program
    {

        static void Main(string[] args)
        {
            SingleModeGame game = new SingleModeGame();
        }

        private static void Game_ActionRequested(object sender, ActionRequestedEventArgs<UserActions> e)
        {
            Console.WriteLine("Action requested");
            var gameObj = sender as Game;
            count += 1;
            Console.ReadKey();
            gameObj.Dispatch(count > 2 ? UserActions.Abort : e.UserAction);
        }
    }
}
