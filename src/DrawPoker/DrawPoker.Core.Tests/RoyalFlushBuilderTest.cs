﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DrawPoker.Core.Entities;
using DrawPoker.Core.CardCombinationBuilders;
using DrawPoker.Core.Interfaces;
using DrawPoker.Core.Enums;

namespace DrawPoker.Core.Tests
{
    [TestClass]
    public class RoyalFlushBuilderTest
    {
        [TestMethod]
        public void RoyalFlushBuilderTest_BuildsProperCombination()
        {
            var suit = Suits.Spades;
            var builder = new RoyalFlushBuilder(suit);
            var cards = TestDataProvider.GetCards(0);
            var combination = builder.Build(cards);

            var cardsOfSuitsCount = combination.Where(x => x.Suit == suit).Count();
            var combinationSet = new HashSet<Card>(combination);

            Assert.AreEqual(5, cardsOfSuitsCount);
            Assert.IsTrue(combinationSet.Contains(new Card(Faces.Ace, suit)));
            Assert.IsTrue(combinationSet.Contains(new Card(Faces.King, suit)));
            Assert.IsTrue(combinationSet.Contains(new Card(Faces.Queen, suit)));
            Assert.IsTrue(combinationSet.Contains(new Card(Faces.Ten, suit)));
            Assert.IsTrue(combinationSet.Contains(new Card(Faces.Jack, suit)));
        }
    }
}
