﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DrawPoker.Core.Entities;
using DrawPoker.Core.ShuffleStrategies;
using DrawPoker.Core.Interfaces;
using DrawPoker.Core.Enums;
using DrawPoker.Core.CardSelectionRules;

namespace DrawPoker.Core.Tests
{
    [TestClass]
    public class DescSeqLoopFaceRuleTests
    {
        [TestMethod]
        public void DescSeqLoopFaceRule_ProvidesSequenceAndLoopsCorrectly()
        {
            var rule = new DescSeqLoopFaceRule(Faces.Two);
            var cards = new Card[]
            {
                new Card(Faces.Ace, Suits.Clubs),
                new Card(Faces.King, Suits.Hearts),
                new Card(Faces.Queen, Suits.Clubs),
                new Card(Faces.Two, Suits.Spades),
                new Card(Faces.Jack, Suits.Diamonds)
            };

            var pick1 = cards.Where(rule.GetPredicate()).First();
            var pick2 = cards.Where(rule.GetPredicate()).First();

            Assert.AreEqual(pick1.Face, Faces.Two);
            Assert.AreEqual(pick2.Face, Faces.Ace);
        }
    }
}
