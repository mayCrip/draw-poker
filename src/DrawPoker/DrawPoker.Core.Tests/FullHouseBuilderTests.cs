﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DrawPoker.Core.Entities;
using DrawPoker.Core.CardCombinationBuilders;
using DrawPoker.Core.Interfaces;
using DrawPoker.Core.Enums;

namespace DrawPoker.Core.Tests
{
    [TestClass]
    public class FullHouseBuilderTests
    { 
        [TestMethod]
        public void FullHouseBuild_BuildsProperCombination()
        {
            var builder = new FullHouseBuilder(Faces.Ten, Faces.Ace);
            var cards = TestDataProvider.GetCards(0);
            var combination = builder.Build(cards);

            var tensCount = combination.Where(x => x.Face == Faces.Ten).Count();
            var acesCount = combination.Where(x => x.Face == Faces.Ace).Count();

            Assert.AreEqual(3, tensCount);
            Assert.AreEqual(2, acesCount);
        }

        [TestMethod]
        public void FullHouseBuild_ReturnNullIfCarsAbsent()
        {
            var builder = new FullHouseBuilder(Faces.Ten, Faces.Ace);
            var cards = TestDataProvider.GetCards(0);
            var allCardsSet = new HashSet<Card>(cards);
            allCardsSet.Remove(new Card(Faces.Ten, Suits.Diamonds));
            allCardsSet.Remove(new Card(Faces.Ten, Suits.Hearts));
            var combination = builder.Build(allCardsSet.ToArray());

            Assert.IsNull(combination);
        }
    }
}
