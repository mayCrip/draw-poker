﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DrawPoker.Core.Entities;
using DrawPoker.Core.ShuffleStrategies;
using DrawPoker.Core.Interfaces;
using DrawPoker.Core.Enums;

namespace DrawPoker.Core.Tests
{
    static class TestDataProvider
    {
        public static Card[] GetCards(int jokerCount)
        {
            var cards = new List<Card>();

            foreach (var face in Enum.GetValues(typeof(Faces)).Cast<Faces>())
            {
                if (face == Faces.Joker) continue;

                foreach (var suit in Enum.GetValues(typeof(Suits)).Cast<Suits>())
                {
                    cards.Add(new Card(face, suit));
                }
            }

            for (int i = 0; i < jokerCount; i++)
            {
                cards.Add(new Card(Faces.Joker, null));
            }

            return cards.ToArray();
        }

        public static IPlayer[] GetPlayers(int count)   
        {
            var players = new List<IPlayer>();

            for (var i = 0; i < count; i++)
            {
                players.Add(new Player(false));
            }

            return players.ToArray();
        }

    }
}
