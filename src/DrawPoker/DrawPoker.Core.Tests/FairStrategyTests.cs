﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DrawPoker.Core.Entities;
using DrawPoker.Core.ShuffleStrategies;
using DrawPoker.Core.Interfaces;
using DrawPoker.Core.Enums;

namespace DrawPoker.Core.Tests
{
    [TestClass]
    public class FairStrategyTests
    {
        private IShuffleStrategy _fairStrategy = new FairStrategy();

        [TestMethod]
        public void FairStrategy_SetsDefaultHandToFiveCards()
        {
            // Arrang
            var cards = TestDataProvider.GetCards(0);
            var player = new Player(false);

            // Act
            var hand = _fairStrategy.ProvideCards(cards, player);

            // Assert
            Assert.AreEqual(hand.Count(), 5);
        }
    }
}
